package us.bowmansarrow

import scala.slick.driver.DerbyDriver.simple._
import Database.threadLocalSession
import org.apache.camel.{Exchange,Processor}
import org.apache.camel.impl.DefaultCamelContext
import org.apache.camel.scala.dsl.builder.RouteBuilder
import org.apache.activemq.camel.component.ActiveMQComponent.activeMQComponent


/**
  * The "main" method. In Scala, extending App is a shortcut for
  * <pre>object Main { def main(args: Array[String]) {...} }</pre>
  */
object Main extends App {
  val dbbean = new DBBean()
  val context = new DefaultCamelContext()
  val jmsFromRouteName = "test-jms:queue:test.queue"
  // using dropbox on the local machine since it syncs with the cloud
  // storage. cheating... kind of
  val dropbox = "file://" + System.getenv("HOME") + "/Dropbox"

  // create derby db and insert some rows
  dbbean.create

  /*
   * Camel in use here, setup 2 routes, 1 is the jms queue provided by
   * activemq, the second is just a starting point for "sending" the
   * csv file to cloud storage (dropbox in this case)
   */
  context.addRoutes(new RouteBuilder {
    // same as from(jmsFromRouteName).to("file:test/cards")
    jmsFromRouteName --> "file:test/cards" // xml files are dropped in
                                           // the test/cards directory

    // route to send a csv file to cloud storage (aka dropbox)
    from("direct:route").process{e =>
      val name = e.getIn.getBody(classOf[String])
        .split(",")(0) // get the player name
        .replaceAll("\"", "") // strip off quotes
        .replaceAll(" ", "_") + ".csv" // replace embedded spaces and
                                       // end with csv
      e.getOut().setBody(e.getIn.getBody(classOf[String]))
      e.getOut().setHeader(Exchange.FILE_NAME, name)
    }.to(dropbox)
  })

  // add the activemq component to the context
  context.addComponent("test-jms",
    activeMQComponent("vm://localhost?broker.persistent=false"))

  val template = context.createProducerTemplate()

  // start the context
  context.start()

  // send all the cards to keep as xml through the mq
  dbbean.keeps foreach {c =>
    template.sendBody(jmsFromRouteName, c.toXml.toString)
  }

  // send all the cards to sell or trade as csv files to my dropbox
  dbbean.sellOrTrade foreach {c =>
    template.sendBody("direct:route", c.toCsv)
  }

  // sleep! gives the file writers time to finish before the context
  // is closed
  Thread.sleep(1000)
  context.stop() // stop the context
  dbbean.destroy // empty the database
}
