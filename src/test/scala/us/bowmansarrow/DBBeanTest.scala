package us.bowmansarrow

import org.scalatest.{BeforeAndAfter, FunSuite, FunSpec, Suite}


class DBBeanTest extends FunSpec with BeforeAndAfter {
  val dbBean = new DBBean()

  before {
    dbBean.create
  }

  after {
    dbBean.destroy
  }

  describe("DBBean tests for methods keeps and sellOrTrade") {
    it ("keeps should return a list of 1 item") {
      assert(dbBean.keeps.length === 1)
    }

    it ("sellOrTrade should return a list of 3 items") {
      assert(dbBean.sellOrTrade.length === 3)
    }
  }
}
