name := "Interview"

version := "1.0-SNAPSHOT"

resolvers ++= Seq(
  "Sonatype Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots",
  "Sonatype Releases" at "http://oss.sonatype.org/content/repositories/releases",
  "apache.snapshots" at "http://repository.apache.org/snapshots"
)

libraryDependencies ++= Seq(
  "org.apache.derby" % "derby" % "10.10.1.1",
  "com.h2database" % "h2" % "1.3.166",
  "com.typesafe.slick" %% "slick" % "1.0.1",
  "ch.qos.logback" % "logback-core" % "1.0.13",
  "org.apache.camel" % "camel-core" % "2.12.1",
  "org.apache.camel" % "camel-scala" % "2.12.1",
  "org.apache.camel" % "camel-jms" % "2.12.1",
  "org.apache.activemq" % "activemq-broker" % "5.8.0",
  "org.apache.activemq" % "activemq-client" % "5.8.0",
  "org.apache.activemq" % "activemq-camel" % "5.8.0",
  "org.slf4j" % "slf4j-log4j12" % "1.7.5",
  "log4j" % "log4j" % "1.2.17",
  "org.scalatest" %% "scalatest" % "1.9.2" % "test"
)
