package us.bowmansarrow

import org.scalatest.{BeforeAndAfter, FunSuite}

class CardTest extends FunSuite {
  val peteRose = Card(1, "Pete Rose", "Reds", "Topps", 4, 1980, 0, 1)

  test ("toCsv should return correct value") {
    val csv =  s"""\"Pete Rose\",\"Reds\",\"Topps\",4,1980,0,1"""
    val actualCsv = peteRose.toCsv
    assert(actualCsv === csv)
  }

  test("toXml should return correct value") {
    val actualXml = peteRose.toXml
    assert((actualXml \\ "playerName").text === "Pete Rose")
    assert((actualXml \\ "teamName").text === "Reds")
    assert((actualXml \\ "cardMaker").text === "Topps")
    assert((actualXml \\ "cardNumber").text.toInt === 4)
    assert((actualXml \\ "cardYear").text.toInt === 1980)
    assert((actualXml \\ "tradeFlag").text.toInt === 0)
    assert((actualXml \\ "sellFlag").text.toInt === 1)
  }
}
