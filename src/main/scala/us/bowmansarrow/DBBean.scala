package us.bowmansarrow

import scala.slick.driver.DerbyDriver.simple._
import Database.threadLocalSession
import org.slf4j.LoggerFactory

/**
  * This is used later to create the database table
  */
object Cards extends Table[Card]("CARDS") {
  def id         = column[Int]     ("id", O.PrimaryKey)
  def playerName = column[String]  ("player_name")
  def teamName   = column[String]  ("team_name")
  def cardMaker  = column[String]  ("card_maker")
  def cardNumber = column[Int]     ("card_number")
  def cardYear   = column[Int]     ("card_year")
  def tradeFlag  = column[Int]     ("trade_flag")
  def sellFlag   = column[Int]     ("sell_flag")
  def * = id ~ playerName ~ teamName ~ cardMaker ~ cardNumber ~ cardYear ~ tradeFlag ~ sellFlag <> (Card, Card.unapply _)
}

/**
  * Like a DAO or some-such, handles specific methods to insert rows
  * into the db, drop the db and find rows based on filters
  */
class DBBean {
  // on-disk derby db
  val db = Database.forURL("jdbc:derby:carddb;create=true",
    driver = "org.apache.derby.jdbc.EmbeddedDriver")

  val logger = LoggerFactory.getLogger(classOf[DBBean])

  // creates the database, the table and inserts 4 rows
  def create() {
    logger.info("creating db")
    db.withSession {
      try {
        Cards.ddl.create
        Cards.insert(Card(1, "Pete Rose", "Reds", "Topps", 4, 1980, 0, 1))
        Cards.insert(Card(2, "Manny Mota", "Dodgers", "Topps", 3, 1980, 1, 1))
        Cards.insert(Card(3, "Pat Putnam", "Rangers", "Topps", 22, 1980, 0, 0))
        Cards.insert(Card(4, "Manny Sarmiento", "Reds", "Topps", 21, 1980, 1, 0))
      } catch {
        case _ : Throwable => println("ddl already created... moving on")
      }
    }
    logger.info("done creating db")
  }

  /**
    * returns all the rows of baseball cards to keep
    */
  def keeps = {
    db.withSession {
      val cards = Query(Cards) filter { c =>
        c.sellFlag === 0 && c.tradeFlag === 0
      }
      cards.list
    }
  }

  /**
    * returns all the rows of baseball cards to either sell or trade
    */
  def sellOrTrade = {
    db.withSession {
      val cards = Query(Cards) filter { c =>
        c.sellFlag === 1 || c.tradeFlag === 1
      }
      cards.list
    }
  }

  /**
    * drops the table from the database
    */
  def destroy():Unit = {
    logger.debug("destroying db")
    db.withSession {
      Cards.ddl.drop
    }
    logger.debug("done destroying db")
  }
}
