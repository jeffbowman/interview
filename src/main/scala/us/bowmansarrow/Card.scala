package us.bowmansarrow

/**
  * Represents a row in the Cards table. Complete with converters to a
  * string, to csv format and to xml
  */
case class Card(id : Int,
  playerName : String,
  teamName   : String,
  cardMaker  : String,
  cardNumber : Int,
  cardYear   : Int,
  tradeFlag  : Int,
  sellFlag   : Int
) {
  /**
    * The same toString you might write in Java
    */
  override def toString() =
    playerName + " " + teamName + " " + cardMaker + " " + cardNumber + " " + cardYear

  /**
    * converter to return csv format
    */
  def toCsv() =
    s"""\"${playerName}\",\"${teamName}\",\"${cardMaker}\",${cardNumber},$cardYear,${tradeFlag},${sellFlag}"""

  /**
    * returns xml (xml is a first-class citizen in Scala)
    */
  def toXml() = {
    <card>
    <playerName>{playerName}</playerName>
    <teamName>{teamName}</teamName>
    <cardMaker>{cardMaker}</cardMaker>
    <cardNumber>{cardNumber}</cardNumber>
    <cardYear>{cardYear}</cardYear>
    <tradeFlag>{tradeFlag}</tradeFlag>
    <sellFlag>{sellFlag}</sellFlag>
    </card>
  }
}
